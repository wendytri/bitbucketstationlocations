### Summary ###
To have basic knowledge and experience on how to use Git with BitBucket. The tutorial can be in here: https://confluence.atlassian.com/bitbucket/tutorial-learn-git-with-bitbucket-cloud-759857287.html

### Setup ###

```
#!Ubuntu

1. Install Git on Linux/Ubuntu by typing the following command in terminal:
   apt-get install git

2. Have a BitBucket account, preferably with academic email to get extra privilege. Then, sign in with your account.
```


### Tutorial ###
**Create repository**

```
#!Text

1. From BitBucket, click the + icon in the global sidebar and select Repository. Insert the repository name and choose the access level and repository type as desired.
2. Following the tutorial, enter BitbucketStationLocations for the Name field. The URL will be https://bitbucket.org/username/repository_name
```


**Copy your Git repository and Add files
**
```
#!Ubuntu

1. Navigate to your home directory.
   cd ~

2. Create a directory to contain your repositories.
   mkdir repos

3. From the terminal, update the directory you want to work into your new *repos* directory
   cd ~/repos

4. From BitBucket, go to your BitBucketStationLocations repository.

5. Click the + icon in the global sidebar and select Clone this repository.
   Bitbucket displays a pop-up clone dialog. For the purpose of this tutorial, don't change your default protocol.

6. Copy the highlighted clone command.

7. From your terminal window, paste the command you copied from Bitbucket and press Return or manually type:
   git clone https://username@bitbucket.org/username/repository_name

8. Enter your Bitbucket password when the terminal asks for it. If you created an account by linking to Google, use your password for that account.
```

**Add a file to your local repository and put it on BitBucket
**
```
#!Ubuntu

1. Go to your terminal window and navigate to the top level of your local repository.
   cd ~/repos/bitbucketstationlocations/

2. Enter the following line into your terminal window to create a new file with content.
   echo "Earth's Moon" >> locations.txt

3. Get the status of your local repository.
   git status

4. Tell Git to track your new locations.txt file using:
   git add locations.txt
   The git add command moves changes from the working directory to the Git staging area. The staging area is where you prepare a snapshot of a set of changes before committing them to the official history.

5. Check the status of the file.
   git status
   Now you can see the new file has been added (staged) and you can commit it when you are ready. The git status command displays the state of the working directory and the staged snapshot.

6. Commit message
   git commit -m 'Initial commit'
   The git commit takes the staged snapshot and commits it to the project history. Combined with git add, this process defines the basic workflow for all Git users.

7. Go back to your local terminal window and send your committed changes to Bitbucket using
   git push origin master
   Your commits are now on the remote repository (origin).

8. Go to your BitbucketStationLocations repository on Bitbucket and click the Commits item in the sidebar

9. You should see a single commit on your repository. Bitbucket combines all the things you just did into that commit and shows it to you. You can see that the Author column shows the value you used when you configured the Git global file ( ~/.gitconfig).

10. Click the Source option.
    You should see that you have a single source file in your repository, the locations.txt file you just added.
```

**Pull changes from your Git repository on BitBucket Cloud**
```
#!Ubuntu + HTML

1. From your *BitbucketStationLocations* repository, click *Source* to open the source directory.
   Notice you only have one file,  locations.txt, in your directory.

2. From the *Source* page, click *New file* in the top right corner. This button only appears after you have added at least one file to the repository.
   A page for creating the new file opens, as shown in the following image.

3. Enter *stationlocations* in the *filename* field.

4. Select *HTML* from the *Syntax* mode list.

5. Add the following HTML code into the text box:
   <p>Bitbucket has the following space stations:</p>
   <p>
       <b>Earth's Moon</b><br>
       Headquarters
   </p>

6. Click *Commit*. The *Commit message* field appears with the message: *stationlocations created online with Bitbucket*.

7. Click *Commit* under the message field.

8. Open your terminal window and navigate to the top level of your local repository.
   cd ~/repos/bitbucketstationlocations/

9. Enter the *git pull --all* command to pull all the changes from Bitbucket.
   git pull --all
   The  *git pull*  command merges the file from your remote repository (Bitbucket) into your local repository with a single command.

10. Navigate to your repository folder on your local system and you'll see the file you just added.
```

**Use a Git branch to merge a file**
```
#!Ubuntu

1. Go to your terminal window and navigate to the top level of your local repository using the following command:
   cd ~/repos/bitbucketstationlocations/

2. Create a branch from your terminal window.
   git branch future-plans

3. Checkout the new branch you just created to start using it.
   git checkout future-plans
   The *git checkout* command works hand-in-hand with *git branch*. Because you are creating a branch to work on something new, every time you create a new branch (with *git branch*), you want to make sure to check it out (with *git checkout*) if you're going to use it.

4. Search for the *bitbucketstationlocations* folder on your local system and open it. You will notice there are no extra files or folders in the directory as a result of the new branch.

5. Open the *stationlocations* file using a text editor.

6. Make a change to the file by adding another station location:
   <p>Bitbucket has the following space stations:</p>
   <p>
       <b>Earth's Moon</b><br>
       Headquarters
   </p>
   *new changes by adding the following code*
   <p>
       <b>Mars</b><br>
       Recreation Department
   </p>

7. Save and close the file.

8. Enter *git status* in the terminal window.
   git status
   If you entered *git status* previously, the line was On branch master because you only had the one master branch
   Before you stage or commit a change, always check this line to make sure the branch where you want to add the change is checked out.

9. Stage your file.
   git add stationlocations

10. Enter the *git commit* command in the terminal window, as shown with the following:
    git commit stationlocations -m 'making a change in a branch'

11. Go to your terminal window and navigate to the top level of your local repository.
    cd ~/repos/bitbucketstationlocations/

12. Enter the *git status* command to be sure you have all your changes committed and find out what branch you have checked out.
    git status

13. Switch to the *master* branch.
    git checkout master

14. Merge changes from the *future-plans* branch into the *master* branch.
    git merge future-plans
    You've essentially moved the pointer for the master branch forward to the current head.

15. Because you don't plan on using *future-plans* anymore, you can delete the branch.
    git branch -d future-plans
    When you delete *future-plans*, you can still access the branch from *master* using a commit id. For example, if you want to undo the changes added from *future-plans*, use the commit id you just received to go back to that branch.

16. Enter *git status* to see the results of your merge, which show that your local repository is one ahead of your remote repository.
    git status

17. From the repository directory in your terminal window,  enter *git push* *origin master* to push the changes.

18. Click the *Overview* page of your Bitbucket repository, and notice you can see your push in the *Recent Activity* stream.

19. Click *Commits* and you can see the commit you made on your local system.
    Notice that the change keeps the same commit id as it had on your local system. You can also see that the line to the left of the commits list has a straight-forward path and shows no branches.
    That’s because the *future-plans* branch never interacted with the remote repository, only the change we created and committed.

20. Click Branches and notice that the page has no record of the branch either.

21. Click *Source*, and then click the *stationlocations* file.
    You can see the last change to the file has the commit id you just pushed

22. Click the file history list to see the changes committed for this file
```

A little separate github tutorial can be found here https://github.com/wendytri/hello-world, following this tutorial: https://guides.github.com/activities/hello-world/